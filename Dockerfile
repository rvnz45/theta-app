FROM node:12 as Frontend
WORKDIR /theta-fe
COPY /frontend/. /theta-fe
RUN npm install
RUN npm run build

FROM tomcat:9-jdk14-openjdk as Backend
WORKDIR /theta-app
COPY /backend/. /theta-app
RUN mkdir classes
RUN javac -cp .:/theta-app/root/WEB-INF/lib/*:/usr/local/tomcat/lib/* -d /theta-app/classes  $(find /theta-app/src/theta/* | grep .java)

FROM tomcat:9-jdk14-openjdk
WORKDIR /usr/local/tomcat/webapps/ROOT
COPY /backend/root .
COPY --from=backend /theta-app/classes WEB-INF/classes
COPY --from=backend /theta-app/src/quartz.properties WEB-INF/classes
COPY --from=backend /theta-app/src/application.configuration WEB-INF/classes
COPY --from=frontend /theta-fe/dist .

#RUN ls -larth WEB-INF/classes


